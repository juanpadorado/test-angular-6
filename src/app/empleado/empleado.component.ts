import { Component } from '@angular/core';
import { Empleado } from './empleado';

@Component({
    selector: 'empleado',
    templateUrl: './empleado.component.html' 
})

export class EmpleadoComponent {
    public titulo = 'Componente Empleado: ';
    public empleado:Empleado;
    public trabajadores:Array<Empleado>;
    public trabajadorExterno:boolean;
    public color:string;
    public colorSeleccionado:string;

    constructor(){
        this.empleado = new Empleado('Juan Dorado', 22, 'Desarrollador', true);
        this.trabajadores = [
            new Empleado('Juan Dorado', 22, 'Desarrollador', true),
            new Empleado('Camila Dorado', 17, 'Salud', false),
            new Empleado('Herney Dorado', 22, 'prueba', true)
        ];

        this.trabajadorExterno = true;
        this.color = 'green';
        this.colorSeleccionado = '#ccc';
    }

    ngOnInit(){
        console.log(this.empleado);
        console.log(this.trabajadores);
    }

    cambiarExterno(valor){
        this.trabajadorExterno = valor;
    }

    logColorSeleccionado(){
        console.log(this.colorSeleccionado);
    }

}