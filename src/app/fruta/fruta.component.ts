import { Component } from '@angular/core';

@Component({
    selector: 'fruta',
    templateUrl: './fruta.component.html' 
})

export class FrutaComponent {
    public nombre_componente = 'Componente de fruta';
    public listado_fruta = 'Naranjas, manzanas y peras';

    public nombre:string = 'David';
    public edad:number = 66;
    public mayorEdad:boolean = true;
    public trabajos:Array<any> = ['Carpintero', 44, 'Fontanero'];
    comodin:any = 23;

    constructor(){
        
    }

}