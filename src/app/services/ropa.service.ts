import { Injectable } from "@angular/core";

@Injectable()

export class RopaService{

    public nombrePrenda = 'Pantalones vaqueros';
    public coleccionRopa = ['Pantalones blancos', 'Camisa roja'];

    prueba(nombrePrenda){
        return nombrePrenda;
    }

    addRopa(nombrePrenda:string):Array<string>{
        this.coleccionRopa.push(nombrePrenda);

        return this.getRopa();
    }

    getRopa(){
        return this.coleccionRopa;
    }

    deleteRopa(index:number):Array<string>{
        this.coleccionRopa.splice(index, 1);
        return this.getRopa();
    }
}