import { Component, OnInit } from '@angular/core';
import { RopaService } from '../services/ropa.service';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [RopaService]
})
export class HomeComponent implements OnInit {

  public titulo = 'Página principal';
  public listadoRopa:Array<string>;
  public prendaGuardar:string;
  public fecha;

  constructor(
    private _ropaService: RopaService
  ) {}

  ngOnInit() {
    this.listadoRopa = this._ropaService.getRopa();
    console.log(this._ropaService.prueba('Camiseta nike'));
  }

  guardarPrenda(){
    this._ropaService.addRopa(this.prendaGuardar);
    this.prendaGuardar = null;
  }

  eliminarPrenda(index:number){
    this._ropaService.deleteRopa(index);
  }

}
