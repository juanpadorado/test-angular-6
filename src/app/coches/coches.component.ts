import { Component, OnInit } from '@angular/core';
import { Coche } from './coche';
import { PeticionesService } from '../services/peticiones.service';

@Component({
    selector: 'coches',
    templateUrl: 'coches.component.html',
    providers: [PeticionesService]
})

export class CochesComponent implements OnInit {
    public coche: Coche;
    public coches:Array<Coche>;
    public articulos;
    
    constructor(
        private _peticionesService: PeticionesService
    ) {
        this.coche = new Coche('', 0, '');
        this.coches = [
            new Coche('Seat panda', 120, 'Blanco'),
            new Coche('Renaul 4', 110, 'Azul')
        ];
     }

    ngOnInit() { 
        this._peticionesService.getArticulos().subscribe(
            result => {
                this.articulos = result;
                
                if(!this.articulos){
                    console.log('error en el servidor');
                }
            },
            error => {
                console.log(error);
            }
        );
    }

    onSubmit() {
        this.coches.push(this.coche);
        this.coche = new Coche('', 0, '');
    }
} 