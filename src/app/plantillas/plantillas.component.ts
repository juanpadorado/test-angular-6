import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'plantillas',
    templateUrl: 'plantillas.component.html'
})

export class PlantillasComponent implements OnInit {
    
    public titulo;
    public administrador;

    constructor() {
        this.titulo = 'Plantillas ngTemplate en Angular';
        this.administrador = true;
     }

    ngOnInit() { }

    cambiar(value){
        this.administrador = value;
    }
}